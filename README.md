# Xylophone

This project serves as a learning process to learn Flutter. It was created based on Udemy course.

Reference: https://www.udemy.com/course/flutter-bootcamp-with-dart

## What you will learn

- How to incorporate open source libraries of code into your project using Flutter Packages.
- How to play sound on both iOS and Android.
- How to generate repeated user interface Widgets.
- How to use Dart functions that can take input arguments as well as return an output.
- Dart arrow syntax for writing one line functions.

## Screenshot

![Xylophone](docs/screenshot.jpg)
